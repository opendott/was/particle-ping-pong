/*
 * Project pong
 * Description: Publish/Subscribe Ping Pong
 * Author: Jens Alexander Ewald <jens@lea.io>
 * Date: 09/10/2019
 */

int i = 0;

void ping()
{
  Particle.publish("ping", "123", PRIVATE);
}

void myHandler(const char *event, const char *data)
{
  i++;
  Serial.print(i);
  Serial.print(event);
  Serial.print(", data: ");
  if (data)
    Serial.println(data);
  else
    Serial.println("NULL");
  digitalWrite(D7, HIGH);
  delay(250);
  digitalWrite(D7, LOW);
  ping();
}

void setup()
{
  pinMode(D7, OUTPUT);
  Particle.subscribe("pong", myHandler, MY_DEVICES);
  Serial.begin(9600);
}

void loop() {
}