[![pipeline status](https://gitlab.com/opendott/was/particle-ping-pong/badges/master/pipeline.svg)](https://gitlab.com/opendott/was/particle-ping-pong/commits/master)

# Playing Ping Pong with Particle Photons

This code is derived from the Particle reference on publishing and subscribing to events, dipatched throught the Particle cloud.

The ping application just responds to a "ping" event and the pong application additionally kicks of the process by sending an initial "pong" event after the particle has successfully connected to the cloud.

The code serves as a simple demostration how to send events and data from one Photon to another even if they are in two different locations (and, well, connected to the internet of course).

---

This code is part of my explorations on Wearables and the Self within the [OpenDoTT Project](https://opendott.org)
